
This SOP is deprecated.

# Howto Create a Tool in VRE
This note is for Communities' contributors that need this information.
    
## Scope   
Here we describe the way to manage the `VRE db` in order to configure (create and/or modify) a new registry in  the Tools collection (benchmarking workflow). This is not to be mistaken with Participant's tool.
   
## Description
Metadata is stored into local VRE storage:
- Communities' tools are stored in `Tools Repository`, which is a `MongoDB database`. 
- `Public reference's data` and `User's data` are also stored in their own repositories.   

However, when creating and/or updating a benchmarking workflow the `VRE VM tool`, an openVRE wrapper, must be also created accordingly with the required data to access the MongoDB database, the input files, and the output directories, and to execute the benchmarking event.     
    
Thus, the VRE Tool must essentially contain the following data: 
- Input ids pointers for Community's specific metadata (`benchmarking event id` and `workflow id`) and nextflow repository directory
- Input files (metrics golden data, aggregation benchmark data)
- Output ids pointers (`Participant's id`, `Challenge id`, and `Community's id`) 
- Output files (assessment results, consolidation results, statistics, and visualizers)
- Path to Nextflow executor
- Nextflow repo image (hash)
   
  
## Requirements
- Access to the VRE MongoDB called `Production VRE` (VRE-Prod).   
- MongoDB GUI recommeded (e.g. Robo3T) ~~(_See installation and configuration SOP #..._) ~~
   
## Procedure
Connect to the VM VRE-Prod, through ssh tunnel (Robo3T) to the oeb-vre host `oeb-vre-workflows` (via `oeb-login.bsc.es`).

The new Community's tool (from now on Tool) must have its specific folder:  

        oeb-submissions/Collections/tools/Tool

and follow the predefined `json` skeleton.    
   
Therefore, you can `expand recursively` any preexisting tool in VRE-Prod to view the tool skeleton.    
Inside Tool there is a collection of fields, as shown in next json schema.

<details><summary>Click to expand commented json schema</summary>


```json
{
    "_id" : "Tool_id",
    "name" : "Tool name",
    "title" : "Name of the Benchmarking Event",
    "short_description" : "Short description of the benchmarking",
    "long_description" : "",
    "owner" : {
        "institution" : "",
        "author" : "",
        "contact" : "email address"
    },
    "external" : true,  # Boolean
    "keywords" : [ 
        "",             # as many fields as required for related keywords
        ""
    ],
    "keywords_tool" : [ 
        "",             # as many fields as required for related keywords 
        ""
    ],
    "status" : NumberLong(1),   # integer, depending on the Community's level ???????????????
    "infrastructure" : {
        "memory" : ,
        "cpus" : ,
        "executable" : "/home/vre/projects/vre-process_nextflow-executor-TMP/VRE_NF_RUNNER", # path to nextflow executor
        "clouds" : {
            "life-bsc" : {
                "launcher" : "SGE",
                "queue" : "default.q"
            }
        }
    },
    "input_files" : [ 
        {
            "name" : "input",
            "description" : "data type to evaluate",
            "help" : "data type description",
            "file_type" : [         
                "BED",
                "CSV", 
                "NEXUS",
                "NEWICK",
                "ORTHOXML",
                "TAR",
                "TSV",
                "TXT"       # any input file(s) that can be supported by the script
            ],
            "data_type" : [ 
                "participant"
            ],
            "required" : true,
            "allow_multiple" : false
        }
    ],
    "input_files_public_dir" : [ 
        {
            "name" : "goldstandard_dir",
            "description" : "Folder where golden data is located",
            "help" : "Folder where metrics golden data is located",
            "type" : "hidden",
            "value" : "metrics_reference/Tool/",
            "file_type" : [ 
                "TAR"
            ],
            "data_type" : [ 
                "metrics_reference"
            ],
            "required" : true,
            "allow_multiple" : false
        }, 
        {
            "name" : "assess_dir",
            "description" : "Folder where benchmark data is located",
            "help" : "Folder where benchmark data is located",
            "type" : "hidden",
            "value" : "aggreggation/Tool/",
            "file_type" : [ 
                "TAR"
            ],
            "data_type" : [ 
                "aggregation"
            ],
            "required" : true,
            "allow_multiple" : false
        }, 
        {
            "name" : "public_ref_dir",
            "description" : "Folder where reference data is located",
            "help" : "Folder where reference data is located",
            "type" : "hidden",
            "value" : "public_reference/",
            "file_type" : [ 
                "TAR"
            ],
            "data_type" : [ 
                "public_reference"
            ],
            "required" : true,
            "allow_multiple" : false
        }
    ],
    "input_files_combinations" : [ 
        {
            "description" : "Evaluate 'data type'",
            "input_files" : [ 
                "input"
            ]
        }
    ],
    "input_files_combinations_internal" : [ 
        [ 
            {
                "participant" : 1
            }
        ]
    ],
    "arguments" : [ 
        {
            "name" : "nextflow_repo_uri",
            "description" : "Nextflow Repository URI",
            "help" : "Nextflow Repository (i.e https://github.com/prj/reponame)",   # to be completed withy the reponame 
            "type" : "hidden",
            "value" : "https://github.com/...", # to be completed with the url
            "required" : true
        }, 
        {
            "name" : "nextflow_repo_tag",
            "description" : "Nextflow Repository tag",
            "help" : "Nextflow Repository Tag version",
            "type" : "hidden",
            "value" : "hash #",
            "required" : true
        }, 
        {
            "name" : "challenges_ids",
            "description" : "Benchmarking Challenges Identifiers",
            "help" : "Identifier for the benchmarking event",
            "type" : "enum_multiple",
            "enum_items" : {
                "description" : [ 
                    "", 
                    ""      # List all challenges within the event
                ],
                "name" : [ 
                    "", 
                    ""      # List all challenges' names within the event
                ],
                ]
            },
            "required" : true,
            "default" : [ 
                ""          # indicate default challenge. It can be ALL
            ]
        }, 
        {
            "name" : "participant_id",
            "description" : "Participant Id.",
            "help" : "Set a participant identifier as a label for your results (e.g. 'myMethod'). Format: String with no special characters. Length: no more than 20 characters (for visualization purposes)",
            "type" : "string",
            "required" : true,
            "default" : ""
        }, 
        {
            "name" : "go_evidences",
            "description" : "GO specific parameters",
            "help" : "Specific parameter for GO challenge",
            "type" : "hidden",
            "required" : true,
            "value" : "exp"
        }
    ],
    "output_files" : [ 
        {
            "name" : "assessment_results",
            "required" : true,
            "allow_multiple" : true,
            "file" : {
                "file_type" : "JSON",
                "file_path" : "assessment_datasets.json",
                "data_type" : "assessment",
                "compressed" : "null",
                "meta_data" : {
                    "description" : "Metrics derivated from the given input data",
                    "tool" : "QfO_6",
                    "visible" : true
                }
            }
        }, 
        {
            "name" : "data_model_export_dir",
            "required" : true,
            "allow_multiple" : true,
            "file" : {
                "file_type" : "JSON",
                "file_path" : "consolidated_results.json",
                "data_type" : "OEB_data_model",
                "compressed" : "null",
                "meta_data" : {
                    "description" : "Set of JSON file ready to be pushed into OEB database",
                    "tool" : "QfO_6",
                    "visible" : true
                }
            }
        }, 
        {
            "name" : "tar_view",
            "required" : true,
            "allow_multiple" : false,
            "custom_visualizer" : true,
            "file" : {
                "file_type" : "TAR",
                "data_type" : "tool_statistics",
                "compressed" : "gzip",
                "meta_data" : {
                    "description" : "Data for metrics visualizer",
                    "tool" : "QfO_6",
                    "visible" : false
                }
            }
        }, 
        {
            "name" : "tar_nf_stats",
            "required" : false,
            "allow_multiple" : false,
            "file" : {
                "file_type" : "TAR",
                "data_type" : "workflow_stats",
                "compressed" : "gzip",
                "meta_data" : {
                    "description" : "Execution monitoring and logging data",
                    "tool" : "QfO_6",
                    "visible" : true
                }
            }
        }, 
        {
            "name" : "tar_other",
            "required" : false,
            "allow_multiple" : false,
            "file" : {
                "file_type" : "TAR",
                "data_type" : "other",
                "compressed" : "gzip",
                "meta_data" : {
                    "description" : "Other execution associated data",
                    "tool" : "QfO_6",
                    "visible" : false
                }
            }
        }, 
        {
            "name" : "report_images",
            "required" : false,
            "allow_multiple" : true,
            "file" : {
                "file_type" : "IMG",
                "data_type" : "report_image",
                "meta_data" : {
                    "description" : "Execution monitoring and logging images",
                    "tool" : "QfO_6",
                    "visible" : true
                }
            }
        }
    ],
    "has_custom_viewer" : true,
    "community-specific_metadata" : {
        "benchmarking_event_id" : "^OEBE[0-9]{3}0[A-Z0-9]{6}$",  # The numbers are assigned by the API consecutively (by adding one)
        "workflow_id" : "^OEBT[0-9]{3}t[A-Z0-9]{6}$",
        "publication_scope" : "oeb",
        "max_requests" : 1,
        "publishable_files" : [ 
            "participant", 
            "OEB_data_model", 
            "tool_statistics"
        ]
    }
}
```
</details>
   

   
**A. CREATE**   
When creating  the Tool (benchmarking workflow) you can use any other preexisting tool as template (e.g. QfO or APAeval). You can copy it in Robo3T GUI, and then you can update the fields (name, id, title, description, owner, keywords,... ) from the new copy by using Robo3T shell window for edition.  
      
**B. MODIFY / UPDATE**   
In the same way, when there is need to modify the Tool the values to be changed can be directly modified by using the shell window from Robo3T.
First check what has been changed: 
- If just a docker step (validation, metrics or consolidation), then create the docker images again(4) and change the commit tag in VRE db.
- If golden data has been updated, upload in the public folder the correct one (3)




![Robo3T GUI](https://drive.google.com/file/d/1acWr1mSWYudepnChmq76Obu8xSSruE97/view?usp=sharing)


<img src="https://drive.google.com/file/d/1acWr1mSWYudepnChmq76Obu8xSSruE97/view?usp=sharing" />

~~**Create a folder with images within GitHub repo instead of google docs link??????    TBC**~~
   

See the json schema above to review which fields should be edited in Tool. 
As a summary:   
- input_files {file_type} attribute: defines the format of the input the workflow takes. It is an array so you can define there as many types as necessary (txt, bed, tsv ..)
- input_files_public_dir {value}: path where golden data is located
- arguments: here you specify the github repo where is the workflow and the commit hash you want to use
- arguments {challenges_ids}: list of challenges that allow the user to select them in VRE before running the Benchamarking.
- nextflow_repo_reldir: the location where the nextflow file is located in githut repo, the relative path to it
ouput_files-> just update the corresponding tool
- community-specific_metadata:  OEB metadata. Used for publication worklfow and to do the mapping with VRE and OEB

After performing this procedure VRE-Prod is ready for accessing your Tool.   
You should test the Workflow to check the update is working. 

---
   
Notice that in parallel the `VM oeb-vre-workflows` must be modified accordingly.   
See [SOP #]() ~~for creating a new tool in VM oeb-vre-workflows.   ~~

See [SOP #]() ~~Howto create a docker image locally   ~~

Fill public_ref_dir folder and metrics_folder in the VM oeb-vre-workflows (/gpfs/VRE/public…)
Create docker images locally if the community doesnt have them in dockerhub.
Clone their repo where the docker images are in VM oeb-vre-workflows, and create them:
docker images -a
sudo bash build.sh <tag>
Test the workflow! 

How to update a workflow in VRE
Check what have been changed. If just a docker step (validation, metrics or consolidation), then create the docker images again(4) and change the commit tag in VRE db.
If golden data has been updated, upload in the public folder the correct one (3)

   
Make also sure that there are `Public reference's data` and `User's data repositories` already created (specially for new events and/or tools).  
See [SOP #]() ~~for creating a new tool in VRE MongoDB database.  ~~  
See [SOP #]() ~~for creating a new tool in VRE MongoDB database.   ~~
   
How to manage VRE data
metadata in MongoDB
How to upload all input files to ...... gpfs
L1 primero se sube a sandbox y luego a staged
L2

   

/home/vre/

For supplementary information you can watch this ![tutorial](https://drive.google.com/drive/folders/1S00SLPKRG6Bw_x_ekTgEcN1E4NJQ8uOh?usp=sharing).

For supplementary information on the data model you can also consult the [Data Model](https://github.com/inab/benchmarking-data-model/tree/2.0.x/json-schemas/2.0.x)
