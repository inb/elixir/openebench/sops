# Howto <SOP Title>

This note is for 'expected user that needs the SOP'.

For example:
This note is for Communities' contributors that need this information.
This note is for either communities' contributors or other APIs that need this information
    
## Scope   
Here we describe the way to ....
   
## Description

Short and long description of what has to be performed. 
 
## Requirements

Requirements and preconditions: needed OpenEBench roles and privileges (internal to OpenEBench or external roles, privileges or powers in other systems, like a database system).

If there is no specific requirements do not remove this section, you can use the following sentence: "There is no requirements for this procedure". This sentence can be complemented by the 
reason because there is no requirements.
   
## Procedure

Description of the procedure: steps, tools involved, common pitfalls, tweaks, tips and tricks ...

If the procedure include several steps number the steps using the following format

### Step 1: <short step title>

### Step 2: <short step title>

### Step 3: <short step title>

### Step ...

## Further information

In this section we include the link to the documentation related to this SOP (technical and user documentation)


# Maintainer(s)

# Related SOPs

Include the link the the related SOP, using the SOP title.
