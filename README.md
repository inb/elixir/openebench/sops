# OpenEBench SOPs

## What it is an SOP

An standard operating procedure is any internal maintenance operation which should be needed in any of
the OpenEBench subprojects along their lifecycles. These procedures might involve both tools and manual
intervention. ([see also Wikipedia definition](https://en.wikipedia.org/wiki/Standard_operating_procedure))

## Structure of an OpenEBench SOP

Any SOP should be available in this repository as a separated MarkDown document.

1. Title
2. Scope(s)
3. Short and long description of what has to be performed.
4. Requirements and preconditions: needed OpenEBench roles and privileges (internal to OpenEBench or external roles, privileges or powers in other systems, like a database system).
5. Description of the procedure: steps, tools involved, common pitfalls, tweaks, tips and tricks ...
6. Maintainer(s).
7. Related SOPs.

[Use the template file](https://gitlab.bsc.es/inb/elixir/openebench/sops/-/blob/main/template.md)

## OpenEBench SOPs

### Intranet Functionalities
* Challenge Participant (Any logged user in VRE by default has this role. Participants that execute benchmarking workflows and keep the results PRIVATE - only for VRE)
  * Accept being findable in the system
  * Follow a Community → future intranet in OEB
  * Unfollow a Community  → future intranet in OEB
  * Upload data into my workspace (Get Data) → VRE
    * Upload data into my data table
    * Create a new file
    * Extract data from external URL
  * List my Workspace (User Workspace) → VRE
  * Run benchmarking workflows (User Workspace) → VRE
  * Request being contributor into a Challenge → future intranet in OEB
  * Delete account
* Communities:
  * List
  * Create (level 1, 2 or 3)
  * Modify
  * Delete
  * Close
  * Add / remove community owners
  * Follow a Community
  * Unfollow a Community
* Event:
  * List
  * Create
  * Modify
  * Delete
  * Close
  * Remove contributor from specific event
  * Show contributors associated with
  * Request delete my Events
  * Accept / reject remove an event
  * Add / remove manager events
* Challenge:
  * List
  * Create
  * Modify
  * Delete
  * Close
  * Request being contributor
  * Manage retractions and withdrawals
  * Accept / reject participants as contributors (including batch processing)
  * Add new participants as contributors
* Tools:
  * List
  * Create (checking don't exist)
  * Modify?
  * _(Delete)_
* Users:
  * List
  * Create
  * Modify
  * Delete
  * Accept being findable in the system
  * Request delete account
  * Accept / reject contributors delete the account
  * Accept / reject benchmarking manager remove the account
  * Accept / reject community owner remove the account
* Roles:
  * List
  * Create
  * Modify
  * Delete
  * Request to leave Manager Role
  * Request to leave Community Owner Role
  * Accept / reject Communities owners to leave role
  * Accept / reject managers to leave role
* Datasets
  * List
  * Create
  * Modify
  * Delete

### OEB (classify by levels???)
* Request participation (login): how an external person asks to be `Participant` in OpenEBench.
* Upload Datasets to OpenEBench (Level 1).
* Validate JSONs according to Data Model.
* Upload contributor's data to sandbox (Level 2)
* Migrate contributor's data from sandbox to staged Database (Level 2)
* How to contribute to the OEB ReadTheDocs

### Communities
* Community engagement (1st contact).
* How to create a Benchmarking workflow.
* How to run a Benchmarking Workflow locally.

### Data expiration protocol
* Scientfic Benchmarking.
* Technical Monitoring.
* VRE

### VRE
* How to evaluate your Tool.
* How to publish results to OEB.
* How to publish results to EUDAT.
* How to register EUDAT account and check it.
* How to upload the benchmarking workflow to VRE.
* How to upload datasets to the specific database related to a workflow.

### System Administration
* How to deploy MongoDB (https://gitlab.bsc.es/inb/mongodb_howto).
* How to backup and restore the MongoDB database (VRE & Technical Monitoring & Scientific Benchmarking).
* How to migrate from one benchmarking data model release to a newer one.
* How to manage OEB data in mongoDB (databases and collections).
* How to manage VRE data.
* How to migrate software from DEV to Production.
* How to deploy Keycloak.
* How to create a virtual machine in Starlife.
* How to create a virtual machine in OpenStack.
* How to obtain a DNS entry for a virtual machine.
* How to deploy OEB portal.
* How to deploy scientific benchmarking API.
* How to deploy visualisation API (bench_event_api).
* How to deploy technical monitoring API.
* How to deploy technical monitoring cron jobs (metrics update).
* How to deploy OEB VRE --> Laia tiene el del OpenVRE.
* How to deploy Software Observatory.
* How to deploy Software Observatory API.

### APIs
* Get Data from OEB Repos.

### Licenses
* OSS License selection (recommendation)
* OSS License compatibility checking 
  * Java
  * PHP
  * Phyton
  * Go
  * javascript
  * complete as necessary

### Miscellaneous
* Definition of data repositories (sandbox and staged - view production).







